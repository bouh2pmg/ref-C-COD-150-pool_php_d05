<?php

include_once("Animal.php");

class Canary extends Animal
{
    private $eggs;

    public function __construct($name)
    {
        parent::__construct($name, 2, Animal::BIRD);
        echo "Yellow and smart ? Here I am !\n";
        $this->eggs = 0;
    }

    public function layEgg()
    {
        ++$this->eggs;
    }

    public function getEggsCount()
    {
        return $this->eggs;
    }
}