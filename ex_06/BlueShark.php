<?php

include_once("Shark.php");

class BlueShark extends Shark
{
    public function __construct($name)
    {
        parent::__construct($name);
    }

    public function eat($food)
    {
        if ($food instanceof Animal && $food != $this && $food->getType() == "fish")
            {
                echo $this->name . " ate a " . $food->getType() . " named " . $food->getName() . ".\n";
                $this->frenzy = false;
            }
        else
            echo $this->name . ": It's not worth my time.\n";

    }
}