<?php

include_once("Shark.php");
include_once("Canary.php");

class Greatwhite extends Shark
{
    public function __construct($name)
    {
        parent::__construct($name);
    }

    public function eat($food)
    {
        if ($food instanceOf Canary)
            {
                echo $this->name . ": Next time you try to give me that to eat, I'll eat you instead.\n";
            }
        if ($food instanceof Animal && $food != $this && !($food instanceOf Canary))
            {
                if ($food instanceOf Shark)
                    echo $this->name . ": The best meal one could wish for.\n";
                echo $this->name . " ate a " . $food->getType() . " named " . $food->getName() . ".\n";
                $this->frenzy = false;
            }
        else
            echo $this->name . ": It's not worth my time.\n";

    }
}